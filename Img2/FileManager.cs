﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Img2
{
    public class FileManager
    {
        /// <summary>
        /// Returns filename to temporary jpg file.
        /// </summary>
        /// <returns>file path</returns>
        private static List<String> tempfiles = new List<string>();
        public static string GetNewJPGTempFilename()
        {
            string tmp = System.IO.Path.GetTempPath() + Guid.NewGuid().ToString() + ".jpg";
            tempfiles.Add(tmp);
            return tmp;
        }

        public static string GetNewTempFilename(string extention)
        {
            if (!extention.StartsWith("."))
            {
                if (!extention.StartsWith("*")) extention = extention.Substring(1);
                else extention = "." + extention;
            }
            string tmp = System.IO.Path.GetTempPath() + Guid.NewGuid().ToString() + extention;
            tempfiles.Add(tmp);
            return tmp;
        }

        public static void ClearTempFiles()
        {
            try
            {
                foreach (var tmpfile in tempfiles)
                {
                    if (File.Exists(tmpfile))
                    {
                        File.Delete(tmpfile);
                    }
                }
            }
            catch (Exception e)
            {
                Debug.WriteLine(e.Message);
            }
        }
    }
}
