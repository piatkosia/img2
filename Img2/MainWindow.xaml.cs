﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
//using System.Windows.Shapes;
using FreeImageAPI;
using Microsoft.Win32;

namespace Img2
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private string srcfilename;
        private string midname;
        private string outfilename;
        System.Drawing.Bitmap bmp;
        public MainWindow()
        {
            InitializeComponent();
            old.Visibility = Visibility.Collapsed;
        }


        private void loadButton_Click(object sender, RoutedEventArgs e)
        {
            LoadImage();
            ReloadImage();
            opensrc.IsEnabled = true;
            preview.IsEnabled = true;
            savebutton.IsEnabled = true;
            reload.IsEnabled = true;

        }

        private void ReloadImage()
        {
            var obr = FreeImage.Load(FREE_IMAGE_FORMAT.FIF_JPEG, SaveJPGHelper.AnyToJpg(srcfilename),
                FREE_IMAGE_LOAD_FLAGS.JPEG_FAST);
            outfilename = FileManager.GetNewJPGTempFilename();
            var flags = getImgFlags();
            if (obr.IsNull)
            {
                MessageBox.Show("No FreeImageDLL found");
            }
            FreeImage.Save(FREE_IMAGE_FORMAT.FIF_JPEG, obr, outfilename, flags);
            OutputImage.Source = new BitmapImage(new Uri(outfilename));
        }

        private FREE_IMAGE_SAVE_FLAGS getImgFlags()
        {
            if (subControl.IsChecked == true)
            {
                if (subHigh.IsChecked == true)
                    return (FREE_IMAGE_SAVE_FLAGS) slider.Value | FREE_IMAGE_SAVE_FLAGS.JPEG_SUBSAMPLING_411;
                else if (subMedium.IsChecked == true)
                    return (FREE_IMAGE_SAVE_FLAGS) slider.Value | FREE_IMAGE_SAVE_FLAGS.JPEG_SUBSAMPLING_420;
                else if (subLow.IsChecked == true)
                    return (FREE_IMAGE_SAVE_FLAGS) slider.Value | FREE_IMAGE_SAVE_FLAGS.JPEG_SUBSAMPLING_422;
                else if (subNo.IsChecked == true)
                    return (FREE_IMAGE_SAVE_FLAGS) slider.Value | FREE_IMAGE_SAVE_FLAGS.JPEG_SUBSAMPLING_444;
                else return (FREE_IMAGE_SAVE_FLAGS)slider.Value;
            }
            else return (FREE_IMAGE_SAVE_FLAGS)slider.Value;
        }


        private void LoadImage()
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.Multiselect = false;
            openFileDialog.Filter = ImageDatas.GetImageFilter();
            if (openFileDialog.ShowDialog() == true)
            {

                srcfilename = openFileDialog.FileName;
                SourceImage.Source = new BitmapImage(new Uri(srcfilename));
            }
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            FileManager.ClearTempFiles();
        }

        private void subControl_Checked(object sender, RoutedEventArgs e)
        {
            SetAsOld();
            subHigh.IsEnabled = true;
            subMedium.IsEnabled = true;
            subLow.IsEnabled = true;
            subNo.IsEnabled = true;
        }

        private void SetAsOld()
        {
            old.Visibility = Visibility.Visible;
            reload.Background = new SolidColorBrush(Colors.Red);
            savebutton.IsEnabled = false;

        }

        private void subControl_Unchecked(object sender, RoutedEventArgs e)
        {
            SetAsOld();
            subHigh.IsEnabled = false;
            subMedium.IsEnabled = false;
            subMedium.IsChecked = true;
            subLow.IsEnabled = false;
            subNo.IsEnabled = false;
        }

        private void button2_Click(object sender, RoutedEventArgs e)
        {
            old.Visibility = Visibility.Collapsed;
            reload.Background = loadButton.Background;
            savebutton.IsEnabled = true;
            ReloadImage();
        }

        private void slider_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            if (old!= null) old.Visibility = Visibility.Visible;
            if (reload != null) reload.Background = new SolidColorBrush(Colors.Red);
            if (sliderValueLabel != null) sliderValueLabel.Content = slider.Value.ToString();
        }

        private void opensrc_Click(object sender, RoutedEventArgs e)
        {
             var wnd = new ImageWindow();
                wnd.SetImage(srcfilename);
            wnd.Show();
        }

        private void preview_Click(object sender, RoutedEventArgs e)
        {
            ReloadImage();
            var wnd = new ImageWindow();
            wnd.SetImage(outfilename);
            wnd.Show();
        }

        private void savebutton_Click(object sender, RoutedEventArgs e)
        {
            Microsoft.Win32.SaveFileDialog dlg = new Microsoft.Win32.SaveFileDialog();
            dlg.FileName = "Preview";
            dlg.DefaultExt = ".jpg"; 
            dlg.Filter = "JPEG images (.jpg)|*.jpg"; 
            Nullable<bool> result = dlg.ShowDialog();
            if (result == true)
            {
                System.IO.File.Copy(outfilename, dlg.FileName);
                string filename = dlg.FileName;
            }
        }

        private void Window_Closed(object sender, EventArgs e)
        {
            Environment.Exit(0);
        }
    }
}
